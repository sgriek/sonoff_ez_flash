from zeroconf import ServiceBrowser, Zeroconf
import ipaddress
import requests
import json

######################################
## Make sure these are all correct ###
######################################
# IP address of server
host_ip = '192.168.8.103'
# Webserver port
host_port = '8080'
# Path is relative to server!
firmware_path = 'tasmota-lite.bin'
# Run `sha256sum firmware.bin` in terminal to work out the SHA256 sum
shasum = '7351bd4342b112326472750ad94c9551a16db313715dfc244e078f63d6d14195'

devices = {}

class MyListener:
    def remove_service(self, zeroconf, type, name):
        print("Service %s removed" % (name,))

    def add_service(self, zeroconf, type, name):
        info = zeroconf.get_service_info(type, name)
        props = info.properties
        hostname = info.name
        address = ipaddress.ip_address(info.addresses[0])
        port = info.port
        id = props[b'id'].decode('utf-8')

        if id not in devices.keys():
            devices[id] = {"hostname": hostname, "address": address, "port": port}
            print(f"Found new device {id}")

        print(f"You have {len(devices)} to flash. Press Enter to begin ...")



def main():
    zeroconf = Zeroconf()
    listener = MyListener()
    browser = ServiceBrowser(zeroconf, "_ewelink._tcp.local.", listener)
    try:
        input()
        if devices:
            print("Devices found! Getting info ...")
            for key in devices.keys():
                get_info(key, devices[key]["address"], devices[key]["port"])
                ret = input("Unlock to allow OTA? (y/n) ")
                if ret == 'n':
                    continue
                elif ret == 'y':
                    print("Let's begin")
                else:
                    main()

                worked = unlock_device(key, devices[key]["address"], devices[key]["port"])
                if not worked:
                    print("Process didn't work. Check debug!")
                    continue

                # Offer to begin flashing
                ret = input("Flash device? (y/n) ")
                if ret == 'n':
                    continue
                elif ret == 'y':
                    print("Let's begin")
                else:
                    main()

                worked = flash_device(key, devices[key]["address"], devices[key]["port"])
                if not worked:
                    print("Process didn't work. Check debug!")
                    continue
                else:
                    devices.pop(id,  None)
                    print("Great success! Device removed from list to flash!")
                    print("*"*50)
        else:
            print("No devices found yet. Rerun and wait!")
            exit(1)
    finally:
        zeroconf.close()

def vital_info(id, body):
    data = body['data']
    ssid = data['ssid'] if data['ssid'] is not None else "Not found"
    unlocked = data['otaUnlock'] if data['otaUnlock'] is not None else "Not found"
    print(f"Device {id} has the following stats:\n\tSSID = {ssid}\n\tOTA Unlocked = {unlocked}\n\n")

def get_info(id, address, port):
    data = '{"deviceid": "' + id + '", "data": {}}'
    url=f"http://{address}:{port}/zeroconf/info"
    result = requests.post(url, data=data)

    vital_info(id, json.loads(result.text))

def unlock_device(id, address, port):
    data = '{"deviceid": "' + id + '", "data": {}}'
    url=f"http://{address}:{port}/zeroconf/ota_unlock"
    result = requests.post(url, data=data)
    result = json.loads(result.text)
    if result['error'] == 0:
        print("Unlock successful")
        return True
    else:
        print("Unlock failed")
        return False

def flash_device(id, address, port):

    print(f" *** Flashing {address}:{port} ***")
    data = '{"deviceid": "' + id + '", "data": { "downloadUrl": "http://' + host_ip + ':' + host_port + '/' + firmware_path + '", "sha256sum": "' + shasum + '"}}'
    url=f"http://{address}:{port}/zeroconf/ota_flash"
    result = requests.post(url, data=data)
    result = json.loads(result.text)
    if result['error'] == 0:
        print("Flash request successful")
        return True
    else:
        print("Flash request failed")
        return False


if __name__ == "__main__":
    main()
