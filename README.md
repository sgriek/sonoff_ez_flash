# Sonoff_Ez_Flash

_Straight forward, no frills flashing code to get a Sonoff DIY onto your favourite ESP firmware!_


## Quick Start

1. Clone repository to your computer `git clone https://gitlab.com/confident_fermi/sonoff_ez_flash.git`
2. (Optionally) Create a Python virtual environment and source the virtual environment
3. Run `pip install -r requirements.txt`
4. Prepare firmware in the root of the `firmware` directory
5. Run `docker -itd -p 8080:80 -v "$PWD"/public-html:/usr/local/apache2/htdocs/ httpd:2.4`
6. Create and connect computer to the SSID `sonoffDiy` pass: `20170618sn` network
7. Boot up Sonoff modules
8. run `python flasher.py`
9. Follow prompts

## Why

I spent two days trying to get the Sonoff devices to flash on both a Windows computer (as per the [Tasmota instructions](https://tasmota.github.io/docs/Sonoff-DIY/) and by the source on a Linux laptop.
Got so frustrated I wrote my own code to do the job.

## How

This works by scanning for any [mDNS](https://community.cisco.com/t5/wireless-mobility-documents/basic-theory-behind-mdns/ta-p/3148577) messages coming off a Sonoff that contain the suffix `_ewelink._tcp`.
From there, we can send it a `POST` request asking for its ID and whether it is `otaLocked`.
Lastly, you can tell it to OTA update and where to get the file from!
Simple.


## Detailed Information

When you bridge the DIY pins on the Sonoff device, it will not boot into the ewelink firmware.

Depending on the factory firmware, will depend on the behaviour of the Sonoff device in DIY mode.

### Versions 3.3-3.4

If your device is in version 3.3-3.4, then in DIY mode, it will be placed in client mode and it will try and connect to an access point. The SSID should be "sonoffDiy" (note the capital 'd') with password "20170618sn". 

You can create this simply by using the Hotspot function on your phone, or on Windows 10, you can go to the settings and searching for "Mobile Hotspot" to create one.

### Versions 3.5+

The Sonoff will create an access point called `itead-xxxxxx` where the `xxxxxx` is a unique identifier. 
Connect to this access point and navigate to `http://10.10.7.1` to provide your SSID and password of the network you wish it to connect.

### Discovery

As mentioned above, the Sonoff in DIY mode will repeatedly broadcast an mDNS message which is `xxxx._ewelink._tcp.local` (again the `xxxx` is a unique identifier). Note that for mDNS you **need** to be on the same network as it will not extend beyond the current subnet.

Using the mDNS broadcast you can collect information such as the ID which is needed to authorise command and control, and the IP address so you don't need to dig it out of your DHCP log.
If you wish to view the contained message in the `mDNS` broadcast, in Linux you can run `avahi-browse -t _ewelink._tcp --resolve`.
This will show all your devices containing the string.

### Communication

To communicate with the Sonoff in DIY mode, you need to make `HTTP` `POST` requests with `JSON` data to provide instructions.
To do that manually, you can run `curl -X POST  -d '{"deviceid": "zzzzzzzzzz", "data": {}}' http://xxx.xxx.xxx.xxx/zeroconf/info` where the `zzz's` are the ID retrieved from the mDNS broadcast, and the `yyy's` are the IP address of the Sonoff device.

### Flashing

When you request to flash, you aren't actually transferring information, rather you are passing on the details from where to get the firmware off a web server.
Two specific mentions; you need to have a web server that supports [RangeRequest](https://developer.mozilla.org/en-US/docs/Web/HTTP/Range_requests), and you need to provide the SHA256 sum.

The TL;DR for the range request is that the Sonoff sends a header that request exactly the subset of bytes it wants in chunks. That way, it doesn't need to retrieve the entire lot of bytes in one stream. 
You can test this out by doing a `curl -H "RangeRequest: bytes=0-1023" -I http://xxx.xxx.xxx.xxx` and you should see the content size is 1024 bytes.

The SHA256 sum is a mathematical computation to verify the file was not malformed between transmission and reception.
You can run this command on any Linux system (and Mac) with `sha256sum file.bin` and it will return the sum and file name.
